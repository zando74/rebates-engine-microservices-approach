import express from 'express';
import { json } from 'body-parser';

import 'express-async-errors';
import cookieSession from 'cookie-session';

import { signupRouter } from './routes/signup';
import { showUsersRouter } from './routes/show';
import { indexUserRouter } from './routes/index';
import { updateUsersRouter } from './routes/update';
import { deleteUsersRouter } from './routes/delete';

import { errorHandler, NotFoundError, currentUser } from '@internapsitude/commonrebates';


const app = express();
app.set('trust proxy', true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test'
  })
);
app.use(currentUser);
app.use(signupRouter);
app.use(indexUserRouter);
app.use(showUsersRouter);
app.use(updateUsersRouter);
app.use(deleteUsersRouter);



app.all('*', () => {
  throw new NotFoundError();
});


app.use(errorHandler);

export { app };