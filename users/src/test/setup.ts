import request from 'supertest';
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import { app } from '../app';
import { User } from '../models/user';
import jwt from 'jsonwebtoken';


declare global {
  namespace NodeJS {
    interface Global {
      fakeUserSignIn(): string[],
      fakeAdminSignIn(): string[]
    }
  }
}

jest.mock('../nats-wrapper');
jest.setTimeout(30000);

let mongo: any;
beforeAll(async () => {

  process.env.JWT_KEY = "cledetest";

  mongo = new MongoMemoryServer();
  const mongoUri = await mongo.getUri();

  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  
});

beforeEach(async () => {
  jest.clearAllMocks();
  const collections = await mongoose.connection.db.collections();
  for(let collection of collections){
    await collection.deleteMany({});
  }
  const user = User.build({ email: "SuperAdmin@apsitude.com", password: "adminRebates", role: "ADMIN" });
  await user.save();
});

afterAll(async () => {
  await mongo.stop();
  await mongoose.connection.close();
});


global.fakeUserSignIn = () => {
  // build a JWT Payload
  const payload = {
    id: 'fakeUserId',
    email: 'fakeSimpleUser@gmail.com',
    role: 'USER'
  }
  // Create the JWT !
  const token = jwt.sign(payload, process.env.JWT_KEY!);
  // Build session object
  const session = { jwt: token };

  // Turn that session into JSON
  const sessionJSON = JSON.stringify(session);

  // Take JSON and encode it as base64
  const base64 = Buffer.from(sessionJSON).toString('base64');

  // Return a string thats the cookie with encoded data
  return [`express:sess=${base64}`];
}

global.fakeAdminSignIn = () => {
  // build a JWT Payload
  const payload = {
    id: 'fakeAdminId',
    email: 'fakeAdminr@gmail.com',
    role: 'ADMIN'
  }
  // Create the JWT !
  const token = jwt.sign(payload, process.env.JWT_KEY!);
  // Build session object
  const session = { jwt: token };

  // Turn that session into JSON
  const sessionJSON = JSON.stringify(session);

  // Take JSON and encode it as base64
  const base64 = Buffer.from(sessionJSON).toString('base64');

  // Return a string thats the cookie with encoded data
  return [`express:sess=${base64}`];
}