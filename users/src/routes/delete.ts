import { NotFoundError, requireAdminAuth, BadRequestError } from '@internapsitude/commonrebates';
import express, { Request, Response } from 'express';
import mongoose from 'mongoose';
import { User } from "../models/user";
import { UserDeletedPublisher } from "../events/publishers/user-deleted-publisher";
import { natsWrapper } from '../nats-wrapper';

const router = express.Router();

router.delete('/api/users/:id', requireAdminAuth, async (req: Request, res: Response) => {
  
  const checkId = mongoose.Types.ObjectId.isValid(req.params.id);
  if(!checkId) {
    throw new BadRequestError("bad id");
  }
  const user = await User.findById(req.params.id);

  if(!user) {
    throw new NotFoundError();
  }
  await user.delete();
  new UserDeletedPublisher(natsWrapper.client).publish({
    id: user.id,
    version: user.version
  })

  res.status(200).send({});
});

export { router as deleteUsersRouter };