import { validateRequest, NotFoundError, requireAdminAuth, BadRequestError, RequestValidationError } from '@internapsitude/commonrebates';
import express, { Request, Response } from 'express';
import { body, validationResult } from 'express-validator';
import mongoose from 'mongoose';
import { User } from "../models/user";
import { UserUpdatedPublisher } from '../events/publishers/user-updated-publisher';
import { natsWrapper } from '../nats-wrapper';

const router = express.Router();

router.put('/api/users/:id', requireAdminAuth,
[
  body('email')
  .if(body('email').exists())
  .isEmail()
  .withMessage('Email must be valid'),

  body('password')
  .if(body('password').exists())
  .trim()
  .isLength({ min: 4, max: 20})
  .withMessage('Password must be between 4 and 20 characters'),

  body('role')
  .if(body('role').exists())
  .isIn(['ADMIN','USER'])
  .withMessage('Role must be valid')
], 
async (req: Request, res: Response) => {
  
  const checkId = mongoose.Types.ObjectId.isValid(req.params.id);
  if(!checkId) {
    throw new BadRequestError("bad id");
  }

  const errors = validationResult(req);

  if(!errors.isEmpty()) {
    throw new RequestValidationError(errors.array());
  }



  const user = await User.findById(req.params.id);

  if(!user) {
    throw new NotFoundError();
  }

  if(req.body.email){
    const existingUser = await User.findOne({ email: req.body.email });
    if(existingUser && existingUser.id != user.id) {
      throw new BadRequestError('Email in use');
    }
    user.email = req.body.email;
  }
  if(req.body.password){
    user.password = req.body.password;
  }
  if(req.body.role){
    user.role = req.body.role;
  }

  await user.save();
  new UserUpdatedPublisher(natsWrapper.client).publish({
    id: user.id,
    email: user.email,
    password: user.password,
    role: user.role,
    version: user.version
  })

  res.send(user);

});

export { router as updateUsersRouter };