import express, { Request, Response } from 'express';
import { body, validationResult } from 'express-validator';
import { User } from '../models/user';
import { RequestValidationError, BadRequestError, requireAdminAuth } from '@internapsitude/commonrebates';
import { UserCreatedPublisher } from '../events/publishers/user-created-publisher';
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post('/api/users/signup', requireAdminAuth, [
  body('email')
    .isEmail()
    .withMessage('Email must be valid'),
  body('password')
    .trim()
    .isLength({ min: 4, max: 20 })
    .withMessage('Password must be between 4 and 20 characters'),
  body('role')
  .isIn(['ADMIN','USER'])
    .withMessage('Role must be valid')
],
async (req: Request, res: Response) => {

  const errors = validationResult(req);

  if(!errors.isEmpty()) {
    throw new RequestValidationError(errors.array());
  }

  const { email, password, role } = req.body;
  const existingUser = await User.findOne({ email });
  if(existingUser) {
    throw new BadRequestError('Email in use');
  }

  const user = User.build({email,password,role});
  await user.save();
  new UserCreatedPublisher(natsWrapper.client).publish({
    id: user.id,
    email: user.email,
    password: user.password,
    role: user.role,
    version: user.version
  });

  res.status(201).send(user);

});

export { router as signupRouter };
