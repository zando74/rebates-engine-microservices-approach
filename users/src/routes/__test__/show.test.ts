import request from 'supertest';
import { app } from '../../app';
import { User } from '../../models/user';

import mongoose from 'mongoose';

it("an unauthenticate user can't access to /api/users/:id", async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .get(`/api/users/${id}`)
    .send({})
  expect(401)
})

it("an USER can't access to /api/users/:id", async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .get(`/api/users/${id}`)
    .set('Cookie', global.fakeUserSignIn())
    .send({})
  expect(401)
})

it('an ADMIN can access to /api/users/:id', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .get(`/api/users/${id}`)
    .set('Cookie', global.fakeAdminSignIn())
    .send({});
  expect(200)
});

it('returns a 404 if the user is not found', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
  .get(`/api/users/${id}`)
  .set('Cookie', global.fakeAdminSignIn())
  .send()
  .expect(404);
});


it('returns the user if the user is found', async () => {
  let email = "fakeUser1@test.fr";
  let password = "fakepassword1";
  let role = "USER";
  const user = User.build({email: email, password: password, role: role});
  await user.save();
  const userResponse = await request(app)
    .get(`/api/users/${user.id}`)
    .set('Cookie', global.fakeAdminSignIn())
    .send()
    .expect(200);
  expect(userResponse.body.email).toEqual(email);
  expect(userResponse.body.role).toEqual(role);
  expect(userResponse.body.id).toEqual(user.id);
});

it('returns Bad Request Error on bad id format', async () => {
  await request(app)
  .get('/api/users/averybadidformat')
  .set('Cookie', global.fakeAdminSignIn())
  .send()
  .expect(400)
})