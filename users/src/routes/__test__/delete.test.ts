import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';
import { User } from '../../models/user';
import { natsWrapper } from '../../nats-wrapper';

it("an unauthenticate user can't access to /api/users/:id", async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .delete(`/api/users/${id}`)
    .send({})
  expect(401)
})

it("an USER can't access to /api/users/:id", async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .delete(`/api/users/${id}`)
    .set('Cookie', global.fakeUserSignIn())
    .send({})
  expect(401)
})

it('an ADMIN can access to /api/users/:id', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .delete(`/api/users/${id}`)
    .set('Cookie', global.fakeAdminSignIn())
    .send({});
  expect(400)
});

it('returns a 404 if the user is not found', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
  .delete(`/api/users/${id}`)
  .set('Cookie', global.fakeAdminSignIn())
  .send({})
  expect(404);
});

it('returns a 200 on deleting a user', async () => {
  const cookie = global.fakeAdminSignIn();
  let email = "fakeUser1@test.fr";
  let newEmail = "fakeUser1NewEmail@test.fr";
  let password = "fakepassword1";
  let role = "USER";
  const user = User.build({email: email, password: password, role: role});
  await user.save();

  await request(app)
    .delete(`/api/users/${user.id}`)
    .set('Cookie', global.fakeAdminSignIn())
    .send({})
  expect(200);

  const deleteUser = await User.findById(user.id);
  expect(deleteUser).toBe(null);

});

it('published an event', async () => {
  const cookie = global.fakeAdminSignIn();
  let email = "fakeUser1@test.fr";
  let newEmail = "fakeUser1NewEmail@test.fr";
  let password = "fakepassword1";
  let role = "USER";
  const user = User.build({email: email, password: password, role: role});
  await user.save();

  await request(app)
    .delete(`/api/users/${user.id}`)
    .set('Cookie', global.fakeAdminSignIn())
    .send({})
  expect(200);

  const deleteUser = await User.findById(user.id);
  expect(deleteUser).toBe(null);
  expect(natsWrapper.client.publish).toHaveBeenCalled();
});