import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';
import { User } from '../../models/user';
import { Password } from '../../services/password';
import { natsWrapper } from '../../nats-wrapper';

it("an unauthenticate user can't access to /api/users/:id", async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .put(`/api/users/${id}`)
    .send({
      email: "newemail@test.com",
      password: "lsdsdoz",
      role: "ADMIN"
    })
  expect(401)
})

it("an USER can't access to /api/users/:id", async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .put(`/api/users/${id}`)
    .set('Cookie', global.fakeUserSignIn())
    .send({
      email: "newemail@test.com",
      password: "lsdsdoz",
      role: "ADMIN"
    })
  expect(401)
})

it('an ADMIN can access to /api/users/:id', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .put(`/api/users/${id}`)
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      email: "newemail@test.com",
      password: "lsdsdoz",
      role: "ADMIN"
    });
  expect(200)
});

it('returns a 404 if the user is not found', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
  .put(`/api/users/${id}`)
  .set('Cookie', global.fakeAdminSignIn())
  .send({
    email: "newemail@test.com",
    password: "lsdsdoz",
    role: "ADMIN"
  })
  expect(404);
});

it('returns a 400 if provide an invalid email password or role', async () => {
  const cookie = global.fakeAdminSignIn();
  let email = "fakeUser1@test.fr";
  let password = "fakepassword1";
  let role = "USER";
  const user = User.build({email: email, password: password, role: role});
  await user.save();

  await request(app)
    .put(`/api/users/${user.id}`)
    .set('Cookie', cookie)
    .send({
      email: 'bademail'
    })
    .expect(400);

    await request(app)
    .put(`/api/users/${user.id}`)
    .set('Cookie', cookie)
    .send({
      password: '1'
    })
    .expect(400);

    await request(app)
    .put(`/api/users/${user.id}`)
    .set('Cookie', cookie)
    .send({
      role: 'sds',
    })
    .expect(400);
})

it('put with the same email', async () => {
  const cookie = global.fakeAdminSignIn();
  let email = "fakeUser1@test.fr";
  let password = "fakepassword1";
  let role = "USER";
  const user = User.build({email: email, password: password, role: role});
  await user.save();

  await request(app)
    .put(`/api/users/${user.id}`)
    .set('Cookie', cookie)
    .send({
      email: email
    })
    .expect(200);
  const userAfter = await User.findById(user.id);
  if(!userAfter){
    throw new Error();
  }
  expect(userAfter.email).toBe(email);
})

it('only update email', async () => {
  const cookie = global.fakeAdminSignIn();
  let email = "fakeUser1@test.fr";
  let newEmail = "fakeUser1NewEmail@test.fr";
  let password = "fakepassword1";
  let role = "USER";
  const user = User.build({email: email, password: password, role: role});
  await user.save();

  await request(app)
    .put(`/api/users/${user.id}`)
    .set('Cookie', cookie)
    .send({
      email: newEmail
    })
    .expect(200);
  const userAfter = await User.findById(user.id);
  if(!userAfter){
    throw new Error();
  }
  expect(userAfter.email).toBe(newEmail);
})

it('only update password', async () => {
  const cookie = global.fakeAdminSignIn();
  let email = "fakeUser1@test.fr";
  let password = "fakepassword1";
  let newPassword = "fakepassword1New";
  let role = "USER";
  const user = User.build({email: email, password: password, role: role});
  await user.save();

  await request(app)
    .put(`/api/users/${user.id}`)
    .set('Cookie', cookie)
    .send({
      password: newPassword
    })
    .expect(200);

  const userAfter = await User.findById(user.id);
  if(!userAfter){
    throw new Error();
  }
  const isMatchingPassword = await Password.compare(userAfter.password,newPassword);
  expect(isMatchingPassword).toBe(true);
})

it('only update role', async () => {
  const cookie = global.fakeAdminSignIn();
  let email = "fakeUser1@test.fr";
  let password = "fakepassword1";
  let role = "USER";
  let newRole = "ADMIN";
  const user = User.build({email: email, password: password, role: role});
  await user.save();

  await request(app)
    .put(`/api/users/${user.id}`)
    .set('Cookie', cookie)
    .send({
      role: newRole
    })
    .expect(200);

  const userAfter = await User.findById(user.id);
  if(!userAfter){
    throw new Error();
  }
  expect(userAfter.role).toBe(newRole);
})

it('published an event', async () => {
  const cookie = global.fakeAdminSignIn();
  let email = "fakeUser1@test.fr";
  let newEmail = "fakeUser1NewEmail@test.fr";
  let password = "fakepassword1";
  let role = "USER";
  const user = User.build({email: email, password: password, role: role});
  await user.save();

  await request(app)
    .put(`/api/users/${user.id}`)
    .set('Cookie', cookie)
    .send({
      email: newEmail
    })
    .expect(200);
  const userAfter = await User.findById(user.id);
  if(!userAfter){
    throw new Error();
  }
  expect(userAfter.email).toBe(newEmail);
  expect(natsWrapper.client.publish).toHaveBeenCalled();
});