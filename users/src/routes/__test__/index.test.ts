import request from 'supertest';
import { app } from '../../app';
import { User } from '../../models/user';

const createUsers = async (userNb: Number) => {
  let users = User.find({});
  for(let i=0;i < userNb; i++){
    const user = User.build({
      email: `fakeUser${i}@test.fr`, 
      password: `fakepassword${i}`, 
      role: i % 2 == 0 ? 'USER' : 'ADMIN'
    });
    await user.save();
  }
  users = User.find({});
}

it('has a route handler listening to /api/users for get requests', async () => {
  const response = await request(app)
    .get('/api/users')
    .send({});
  expect(response.status).not.toBe(404);
});

it("an unauthenticate user can't access to /api/users", async () => {
  await request(app)
    .get('/api/users')
    .send({})
  expect(401)
})

it("an USER can't access to /api/users", async () => {
  await request(app)
    .get('/api/users')
    .set('Cookie', global.fakeUserSignIn())
    .send({})
  expect(401)
})

it('an ADMIN can access to /api/users', async () => {
  await request(app)
    .get('/api/users')
    .set('Cookie', global.fakeAdminSignIn())
    .send({})
  expect(200)
});

it('can fetch a list of users', async () => {
  const USERS_NUMBER = 50;
  await createUsers(USERS_NUMBER);
  const response = await request(app)
    .get('/api/users')
    .set('Cookie', global.fakeAdminSignIn())
    .send({})
    .expect(200);
  expect(response.body.length).toEqual(USERS_NUMBER+1);
});