import request from 'supertest';
import { app } from '../../app';
import { User } from '../../models/user';
import { Password } from '../../services/password';
import { natsWrapper } from '../../nats-wrapper';

jest.mock('../../nats-wrapper');

it('has a route handler listening to /api/users/signup for post requests', async () => {
  const response = await request(app)
    .post('/api/users/signup')
    .send({});
  expect(response.status).not.toBe(404);
});

it("an unauthenticate user can't access to /api/users/signup", async () => {
  await request(app)
    .post('/api/users/signup')
    .send({
      email: 'SuperUser@apsitude.com',
      password: 'superPassword',
      role: 'USER'
    })
  expect(401)
})

it("an USER can't access to /api/users/signup", async () => {
  await request(app)
    .post('/api/users/signup')
    .set('Cookie', global.fakeUserSignIn())
    .send({
      email: 'SuperUser@apsitude.com',
      password: 'superPassword',
      role: 'USER'
    })
  expect(401)
})

it('an ADMIN can access to /api/users/signup', async () => {
  await request(app)
    .post('/api/users/signup')
    .set('Cookie', global.fakeAdminSignIn())
    .send({})
  expect(400)
});


it('returns a 201 on successful signup', async () => {
  return request(app)
    .post('/api/users/signup')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      email: 'SuperUser@apsitude.com',
      password: 'superPassword',
      role: 'USER'
    })
    .expect(201);
});

it('the user is stored in database with good information', async () => {
  let email = 'SuperUser3@apsitude.com';
  let password = 'superPassword';
  let role = 'USER';

  await request(app)
    .post('/api/users/signup')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      email: email,
      password: password,
      role: role
    })
    .expect(201);

  const user = await User.findOne({email: email});
  if(!user)
    throw new Error('database store test failed')
  expect(user.email).toEqual(email);
  const isMatchingPassword = await Password.compare(user.password,password);
  expect(isMatchingPassword).toEqual(true);
  expect(user.role).toEqual(role);
})

it('returns a 400 if an invalid email is provided', async () => {
  return request(app)
    .post('/api/users/signup')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      emails: 'SuperUser@apsitude.com',
      password: 'superPassword',
      role: 'USER'
    })
    .expect(400);
  });
it('returns a 400 if an invalid password is provided', async () => {
  return request(app)
    .post('/api/users/signup')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      email: 'SuperUser@apsitude.com',
      passwordp: 'superPassword',
      role: 'USER'
    })
    .expect(400);
});

it('returns a 400 if an invalid Role is provided', async () => {
  return request(app)
    .post('/api/users/signup')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      email: 'SuperUser@apsitude.com',
      password: 'superPassword',
      role: 'Usdsd'
    })
    .expect(400);
});

it('returns a 400 on in use email', async () => {
  await request(app)
    .post('/api/users/signup')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      email: 'SuperUser@apsitude.com',
      password: 'superPassword',
      role: 'USER'
    })
    .expect(201);

  return request(app)
    .post('/api/users/signup')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      email: 'SuperUser@apsitude.com',
      password: 'superPassword',
      role: 'USER'
    })
    .expect(400);
});

it('published an event', async () => {
  await request(app)
  .post('/api/users/signup')
  .set('Cookie', global.fakeAdminSignIn())
  .send({
    email: 'SuperUser@apsitude.com',
    password: 'superPassword',
    role: 'USER'
  })
  .expect(201);
  expect(natsWrapper.client.publish).toHaveBeenCalled();
});