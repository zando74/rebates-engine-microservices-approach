import { BadRequestError, NotFoundError, requireAdminAuth } from '@internapsitude/commonrebates';
import express, { Request, Response } from 'express';
import { User } from '../models/user';
import mongoose from 'mongoose';

const router = express.Router();

router.get('/api/users/:id', requireAdminAuth, async (req: Request, res: Response) => {
  
  const checkId = mongoose.Types.ObjectId.isValid(req.params.id);
  if(!checkId) {
    throw new BadRequestError("bad id");
  }
  const user = await User.findById(req.params.id);

  if(!user) {
    throw new NotFoundError();
  }

  res.send(user);
});

export { router as showUsersRouter }
