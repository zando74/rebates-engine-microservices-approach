import express, { Request, Response } from 'express';
import { User } from '../models/user';
import { requireAdminAuth } from '@internapsitude/commonrebates';

const router = express.Router();

router.get('/api/users', requireAdminAuth, async (req: Request, res: Response) => {
  const users = await User.find({});
  res.send(users);
});

export { router as indexUserRouter };
