import { Publisher, Subjects, UserCreatedEvent } from '@internapsitude/commonrebates';

export class UserCreatedPublisher extends Publisher<UserCreatedEvent> {
  subject: Subjects.UserCreated = Subjects.UserCreated;
}