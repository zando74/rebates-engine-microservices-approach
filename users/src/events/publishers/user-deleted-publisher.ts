import { Publisher, Subjects, UserDeletedEvent } from '@internapsitude/commonrebates';

export class UserDeletedPublisher extends Publisher<UserDeletedEvent> {
  subject: Subjects.UserDeleted = Subjects.UserDeleted;
}