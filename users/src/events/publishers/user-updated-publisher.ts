import { Publisher, Subjects, UserUpdatedEvent } from '@internapsitude/commonrebates';

export class UserUpdatedPublisher extends Publisher<UserUpdatedEvent> {
  subject: Subjects.UserUpdated = Subjects.UserUpdated;
}