import { User } from '../user';

it('implements Optimistic Concurrency Control', async (done) => {
  const user = User.build({
    email: 'test@email.com',
    password: '123456',
    role: 'USER',
  });

  await user.save();

  const firstInstance = await User.findById(user.id);
  const secondInstance = await User.findById(user.id);

  firstInstance!.set({ email: 'newEmail@test.com' });
  secondInstance!.set({ email: 'anotherEmail@test.com' });

  await firstInstance!.save();

  try{
    await secondInstance!.save();
  } catch(err) {
    return done();
  }

  throw new Error('Should not reach this point');
});

it('increments the version number on multiple saves', async () => {
  const user = User.build({
    email: 'test@email.com',
    password: '123456',
    role: 'USER',
  });
  
  await user.save();
  expect(user.version).toEqual(0);
  await user.save();
  expect(user.version).toEqual(1);
  await user.save();
  expect(user.version).toEqual(2);
});