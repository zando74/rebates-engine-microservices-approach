import { NotFoundError, requireAdminAuth, BadRequestError } from '@internapsitude/commonrebates';
import express, { Request, Response } from 'express';
import mongoose from 'mongoose';
import { Amount } from "../models/amount";
import { AmountDeletedPublisher } from "../events/publishers/amount-deleted-publisher";
import { natsWrapper } from '../nats-wrapper';

const router = express.Router();

router.delete('/api/amount/:id', requireAdminAuth, async (req: Request, res: Response) => {
  
  const checkId = mongoose.Types.ObjectId.isValid(req.params.id);
  if(!checkId) {
    throw new BadRequestError("bad id");
  }
  const amount = await Amount.findById(req.params.id);

  if(!amount) {
    throw new NotFoundError();
  }
  await amount.delete();

  res.status(200).send({});
  new AmountDeletedPublisher(natsWrapper.client).publish({
    id: amount.id,
    version: amount.version
  });
});

export { router as deleteAmountRouter };