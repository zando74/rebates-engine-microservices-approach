import { BadRequestError, NotFoundError, requireAdminAuth } from '@internapsitude/commonrebates';
import express, { Request, Response } from 'express';
import { Amount } from '../models/amount';
import mongoose from 'mongoose';

const router = express.Router();

router.get('/api/amount/:id', requireAdminAuth, async (req: Request, res: Response) => {
  
  const checkId = mongoose.Types.ObjectId.isValid(req.params.id);
  if(!checkId) {
    throw new BadRequestError("bad id");
  }
  const amount = await Amount.findById(req.params.id);

  if(!amount) {
    throw new NotFoundError();
  }

  res.send(amount);
});

export { router as showAmountsRouter }