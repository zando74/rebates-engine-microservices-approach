import request from 'supertest';
import { app } from '../../app';
import { Amount } from '../../models/amount';

it('has a route handler listening to /api/amount for post requests', async () => {
  const response = await request(app)
    .get('/api/amount')
  expect(response.status).not.toBe(404);
});

it("an unauthenticate user can't access to /api/amount", async () => {
  await request(app)
    .get('/api/amount')
  expect(401)
})

it("an USER can't access to /api/amount/new", async () => {
  await request(app)
    .get('/api/amount')
    .set('Cookie', global.fakeUserSignIn())
  expect(401)
})

it('an ADMIN can access to /api/amount/new', async () => {
  await request(app)
    .get('/api/amount')
    .set('Cookie', global.fakeAdminSignIn())
  expect(200)
});

it('can fetch a list of amounts', async () => {
  for(let i=0;i<5;i++){
    let amount = Amount.build({
      amountRefTransaction: i,
      name: "Gross Amount"
    });
    await amount.save();
  }
  const response = await request(app)
    .get('/api/amount')
    .set('Cookie', global.fakeAdminSignIn())
    .send({})
    .expect(200);
  expect(response.body.length).toEqual(5);
});