import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';
import { Amount } from '../../models/amount';
import { natsWrapper } from '../../nats-wrapper';

it("an unauthenticate user can't access to /api/amount/:id", async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .delete(`/api/amount/${id}`)
    .send({})
  expect(401)
})

it("an USER can't access to /api/amount/:id", async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .delete(`/api/amount/${id}`)
    .set('Cookie', global.fakeUserSignIn())
    .send({})
  expect(401)
})

it('an ADMIN can access to /api/amount/:id', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .delete(`/api/amount/${id}`)
    .set('Cookie', global.fakeAdminSignIn())
    .send({});
  expect(400)
});

it('returns a 404 if the amount is not found', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
  .delete(`/api/amount/${id}`)
  .set('Cookie', global.fakeAdminSignIn())
  .send({})
  expect(404);
});

it('returns a 200 on deleting a amount', async () => {
  const cookie = global.fakeAdminSignIn();
  let amountRefTransaction = 4;
  let name = "Salaries";
  const amount = Amount.build({amountRefTransaction: amountRefTransaction, name: name });
  await amount.save();

  await request(app)
    .delete(`/api/amount/${amount.id}`)
    .set('Cookie', global.fakeAdminSignIn())
    .send({})
  expect(200);

  const deleteAmount = await Amount.findById(amount.id);
  expect(deleteAmount).toBe(null);

});

it('published an event', async () => {
  const cookie = global.fakeAdminSignIn();
  let amountRefTransaction = 4;
  let name = "Salaries";
  const amount = Amount.build({amountRefTransaction: amountRefTransaction, name: name });
  await amount.save();

  await request(app)
    .delete(`/api/amount/${amount.id}`)
    .set('Cookie', global.fakeAdminSignIn())
    .send({})
  expect(200);

  const deleteAmount = await Amount.findById(amount.id);
  expect(deleteAmount).toBe(null);
  expect(natsWrapper.client.publish).toHaveBeenCalled();
});
