import request from 'supertest';
import { app } from '../../app';
import { Amount } from '../../models/amount';

import mongoose from 'mongoose';

it("an unauthenticate user can't access to /api/amount/:id", async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .get(`/api/amount/${id}`)
    .send({})
  expect(401)
})

it("an USER can't access to /api/amount/:id", async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .get(`/api/amount/${id}`)
    .set('Cookie', global.fakeUserSignIn())
    .send({})
  expect(401)
})

it('an ADMIN can access to /api/amount/:id', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
    .get(`/api/amount/${id}`)
    .set('Cookie', global.fakeAdminSignIn())
    .send({});
  expect(200)
});

it('returns a 404 if the amount is not found', async () => {
  const id = new mongoose.Types.ObjectId().toHexString();
  await request(app)
  .get(`/api/amount/${id}`)
  .set('Cookie', global.fakeAdminSignIn())
  .send()
  .expect(404);
});


it('returns the amount if the amount is found', async () => {
  let amountRefTransaction = 3;
  let name = "Salaries";

  const amount = Amount.build({ amountRefTransaction: amountRefTransaction, name: name });
  await amount.save();
  const amountResponse = await request(app)
    .get(`/api/amount/${amount.id}`)
    .set('Cookie', global.fakeAdminSignIn())
    .send()
    .expect(200);
  expect(amountResponse.body.amountRefTransaction).toEqual(amountRefTransaction);
  expect(amountResponse.body.name).toEqual(name);
  expect(amountResponse.body.id).toEqual(amount.id);
});

it('returns Bad Request Error on bad id format', async () => {
  await request(app)
  .get('/api/amount/averybadidformat')
  .set('Cookie', global.fakeAdminSignIn())
  .send()
  .expect(400)
})