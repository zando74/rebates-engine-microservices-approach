import request from 'supertest';
import { app } from '../../app';
import { Amount } from '../../models/amount';
import { natsWrapper } from '../../nats-wrapper';

it('has a route handler listening to /api/amount/new for post requests', async () => {
  const response = await request(app)
    .post('/api/amount/new')
    .send({});
  expect(response.status).not.toBe(404);
});

it("an unauthenticate user can't access to /api/amount/new", async () => {
  await request(app)
    .post('/api/amount/new')
    .send({
      amountRefTransaction: 1,
      name: 'Gross Amount'
    })
  expect(401)
})

it("an USER can't access to /api/amount/new", async () => {
  await request(app)
    .post('/api/amount/new')
    .set('Cookie', global.fakeUserSignIn())
    .send({
      amountRefTransaction: 1,
      name: 'Gross Amount'
    })
  expect(401)
})

it('an ADMIN can access to /api/amount/new', async () => {
  await request(app)
    .post('/api/amount/new')
    .set('Cookie', global.fakeAdminSignIn())
    .send({})
  expect(400)
});

it('returns a 201 on successful amount creation', async () => {
  return request(app)
    .post('/api/amount/new')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      amountRefTransaction: 1,
      name: 'Salaries'
    })
    .expect(201);
});

it('the amount is stored in database with good information', async () => {
  let amountRefTransaction = 2;
  let name = 'Gross Amount';

  await request(app)
    .post('/api/amount/new')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      amountRefTransaction: amountRefTransaction,
      name: name
    })
    .expect(201);

  const amount = await Amount.findOne({amountRefTransaction: amountRefTransaction});
  if(!amount)
    throw new Error('database store test failed')
  expect(amount.amountRefTransaction).toEqual(amountRefTransaction);
  expect(amount.name).toEqual(name);
});

it("can't create if the Amount reference is in use", async () => {
  let amountRefTransaction = 2;
  let name = 'Gross Amount';
  await request(app)
    .post('/api/amount/new')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      amountRefTransaction: amountRefTransaction,
      name: name
    })
    .expect(201);
  return request(app)
    .post('/api/amount/new')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      amountRefTransaction: amountRefTransaction,
      name: name
    })
    .expect(400);
});

it('returns a 400 if an invalid amountRefTransaction is provided', async () => {
  let amountRefTransaction = 'sdz';
  let name = 'Gross Amount';
  return request(app)
    .post('/api/amount/new')
    .set('Cookie', global.fakeAdminSignIn())
    .send({
      amountRefTransaction: amountRefTransaction,
      name: name
    })
    .expect(400);
});

it('published an event', async () => {
  let amountRefTransaction = 3;
  let name = 'Gross Amount';
  await request(app)
  .post('/api/amount/new')
  .set('Cookie', global.fakeAdminSignIn())
  .send({
    amountRefTransaction: amountRefTransaction,
    name: name
  })
  .expect(201);
  expect(natsWrapper.client.publish).toHaveBeenCalled();
});