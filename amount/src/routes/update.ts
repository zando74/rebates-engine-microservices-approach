import { validateRequest, NotFoundError, requireAdminAuth, BadRequestError, RequestValidationError } from '@internapsitude/commonrebates';
import express, { Request, Response } from 'express';
import { body, validationResult } from 'express-validator';
import mongoose from 'mongoose';
import { Amount } from "../models/amount";
import { AmountUpdatedPublisher } from '../events/publishers/amount-updated-publisher';
import { natsWrapper } from '../nats-wrapper';

const router = express.Router();

router.put('/api/amount/:id', requireAdminAuth,
[
  body('name')
    .not()
    .isEmpty()
    .withMessage('provide a name for the Amount')
],
async (req: Request, res: Response) => {
  const checkId = mongoose.Types.ObjectId.isValid(req.params.id);

  if(!checkId) {
    throw new BadRequestError("bad id");
  }

  const errors = validationResult(req);

  if(!errors.isEmpty()) {
    throw new RequestValidationError(errors.array());
  }

  const amount = await Amount.findById(req.params.id);

  if(!amount) {
    throw new NotFoundError();
  }

  amount.name = req.body.name;
  await amount.save();
  new AmountUpdatedPublisher(natsWrapper.client).publish({
    id: amount.id,
    name: amount.name,
    version: amount.version
  });

  res.send(amount);
});

export { router as updateAmountRouter };