import express, { Request, Response } from 'express';
import { Amount } from '../models/amount';
import { requireAdminAuth } from '@internapsitude/commonrebates';

const router = express.Router();

router.get('/api/amount', requireAdminAuth, async (req: Request, res: Response) => {
  const amount = await Amount.find({});
  res.send(amount);
});

export { router as indexAmountRouter };