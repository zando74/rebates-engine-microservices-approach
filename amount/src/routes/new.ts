import express, { Request, Response } from 'express';
import { body, validationResult } from 'express-validator';
import { Amount } from '../models/amount';
import { RequestValidationError, BadRequestError, requireAdminAuth } from '@internapsitude/commonrebates';
import { AmountCreatedPublisher } from '../events/publishers/amount-created-publisher';
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post('/api/amount/new', requireAdminAuth, [
  body('amountRefTransaction')
    .isIn([1,2,3,4,5])
    .withMessage('amountRefTransaction must be valid'),
  body('name')
    .not()
    .isEmpty()
    .withMessage('provide a name for the Amount')
],
async (req: Request, res: Response) => {

  const errors = validationResult(req);

  if(!errors.isEmpty()) {
    throw new RequestValidationError(errors.array());
  }

  const { amountRefTransaction, name } = req.body;
  const existingAmount = await Amount.findOne({ amountRefTransaction: amountRefTransaction });
  if(existingAmount) {
    throw new BadRequestError('Amount field in use, update it');
  }

  const amount = Amount.build({amountRefTransaction,name});
  await amount.save();
  new AmountCreatedPublisher(natsWrapper.client).publish({
    id: amount.id,
    amountRefTransaction: amount.amountRefTransaction,
    name: amount.name,
    version: amount.version
  });
  
  res.status(201).send(amount);

});

export { router as newAmountRouter };