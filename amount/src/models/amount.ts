import mongoose from 'mongoose';
import { updateIfCurrentPlugin } from 'mongoose-update-if-current';

// An interface that describes the properties
// that are required to create a new Amount
interface AmountAttrs {
  amountRefTransaction: number;
  name: string;
};

// An interface that describes the properties
// that a Amount Model has
interface AmountModel extends mongoose.Model<AmountDoc> {
  build(attrs: AmountAttrs): AmountDoc;
}

// An interface that describes the properties
// that a Amount Document has
interface AmountDoc extends mongoose.Document {
  amountRefTransaction: number;
  name: string;
  version: number;
}

const amountSchema = new mongoose.Schema({
  amountRefTransaction: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true
  }
}, {
  toJSON: {
    transform(doc, ret) {
      ret.id = ret._id;
      delete ret._id;
      delete ret.__v;
    }

  }  
});

amountSchema.set('versionKey', 'version');
amountSchema.plugin(updateIfCurrentPlugin);

amountSchema.statics.build = (attrs: AmountAttrs) => {
  return new Amount(attrs);
}

const Amount = mongoose.model<AmountDoc, AmountModel>('Amount', amountSchema);

export { Amount };