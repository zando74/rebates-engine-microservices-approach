import express from 'express';
import { json } from 'body-parser';

import 'express-async-errors';
import cookieSession from 'cookie-session';

import { newAmountRouter } from './routes/new';
import { indexAmountRouter } from './routes/index';
import { showAmountsRouter } from './routes/show';
import { deleteAmountRouter } from './routes/delete';
import { updateAmountRouter } from './routes/update';

import { errorHandler, NotFoundError, currentUser } from '@internapsitude/commonrebates';


const app = express();
app.set('trust proxy', true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test'
  })
);
app.use(currentUser);
app.use(indexAmountRouter);
app.use(newAmountRouter);
app.use(showAmountsRouter);
app.use(deleteAmountRouter);
app.use(updateAmountRouter);



app.all('*', () => {
  throw new NotFoundError();
});


app.use(errorHandler);

export { app };