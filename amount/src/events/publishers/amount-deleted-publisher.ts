import { Publisher, Subjects, AmountDeletedEvent } from '@internapsitude/commonrebates';

export class AmountDeletedPublisher extends Publisher<AmountDeletedEvent> {
  subject: Subjects.AmountDeleted = Subjects.AmountDeleted;
}