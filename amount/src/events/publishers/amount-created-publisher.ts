import { Publisher, Subjects, AmountCreatedEvent } from '@internapsitude/commonrebates';

export class AmountCreatedPublisher extends Publisher<AmountCreatedEvent> {
  subject: Subjects.AmountCreated = Subjects.AmountCreated;
}