import { Publisher, Subjects, AmountUpdatedEvent } from '@internapsitude/commonrebates';

export class AmountUpdatedPublisher extends Publisher<AmountUpdatedEvent> {
  subject: Subjects.AmountUpdated = Subjects.AmountUpdated;
}