import mongoose from 'mongoose';
import { User } from './models/user';
import { app } from './app';
import { Password } from './services/password';
import { natsWrapper } from './nats-wrapper';
import { UserCreatedListener } from './events/listeners/user-created-listener';
import { UserUpdatedListener } from './events/listeners/user-updated-listener';
import { UserDeletedListener } from './events/listeners/user-deleted-listener';

const start = async () => {
  if(!process.env.JWT_KEY) {
    throw new Error('JWT_KEY must be defined');
  }
  if(!process.env.MONGO_URI) {
    throw new Error('Mongo_URI must be defined');
  }
  if(!process.env.NATS_CLIENT_ID) {
    throw new Error('NATS_CLIENT_ID must be defined');
  }
  if(!process.env.NATS_URL) {
    throw new Error('NATS_URL must be defined');
  }
  if(!process.env.NATS_CLUSTER_ID) {
    throw new Error('NATS_CLUSTER_ID must be defined');
  }

  try {
    await natsWrapper.connect(process.env.NATS_CLUSTER_ID, process.env.NATS_CLIENT_ID, process.env.NATS_URL);
    natsWrapper.client.on('close', () => {
      console.log('NATS connection closed!');
      process.exit();
    });
    process.on('SIGINT', () => natsWrapper.client.close());
    process.on('SIGTERM', () => natsWrapper.client.close());

    new UserCreatedListener(natsWrapper.client).listen();
    new UserUpdatedListener(natsWrapper.client).listen();
    new UserDeletedListener(natsWrapper.client).listen();

    await mongoose.connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    });
    console.log('connected to database');
    const hashed = await Password.toHash("adminRebates");
    const user = User.build({ id: new mongoose.Types.ObjectId().toHexString(), email: "SuperAdmin@apsitude.com", password: hashed, role: "ADMIN" });
    await user.save();
  } catch(err) {
    console.log(err);
  }
  
} 

app.listen(3000, () => {
  console.log('Listening on port 3000 !!!');
});

start();