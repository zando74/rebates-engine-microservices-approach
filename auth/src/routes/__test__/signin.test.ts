import request from 'supertest';
import { app } from '../../app'


it('returns a 200 on successful signin', async () => {
  return request(app)
    .post('/api/auth/signin')
    .send({
      email: 'SuperAdmin@apsitude.com',
      password: 'adminRebates'
    })
    .expect(200);
});

it('returns a 400 with an invalid email', async ()  => {
  return request(app)
    .post('/api/auth/signin')
    .send({
      email: 'SuperAdmhfm',
      password: 'adminRebates'
    })
    .expect(400);
});

it('returns a 400 with an invalid password', async ()  => {
  return request(app)
    .post('/api/auth/signin')
    .send({
      email: 'SuperAdmin@apsitude.com',
      password: 'as'
    })
    .expect(400);
});

it('returns a 400 with invalid request', async ()  => {
  await request(app)
  .post('/api/auth/signin')
  .send({
    email: "SuperAdmin@apsitude.com"
  })
  .expect(400);

  return request(app)
    .post('/api/auth/signin')
    .send({
      password: "avalidpassword"
    })
    .expect(400);
});

it('sets a cookie after successful signin', async() => {
  const response = await request(app)
  .post('/api/auth/signin')
  .send({
    email: 'SuperAdmin@apsitude.com',
    password: 'adminRebates'
  })
  .expect(200);

  expect(response.get('Set-Cookie')).toBeDefined();

})