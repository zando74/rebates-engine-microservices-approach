import express from 'express';
import { json } from 'body-parser';

import 'express-async-errors';
import cookieSession from 'cookie-session';


import { signinRouter } from './routes/signin';
import { currentUserRouter } from './routes/current-user';
import { signoutRouter } from './routes/signout';

import { errorHandler, NotFoundError } from '@internapsitude/commonrebates';


const app = express();
app.set('trust proxy', true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test'
  })
);

app.use(signinRouter);
app.use(currentUserRouter);
app.use(signoutRouter);

app.all('*', () => {
  throw new NotFoundError();
});


app.use(errorHandler);

export { app };