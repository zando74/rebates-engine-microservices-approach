import { Message } from 'node-nats-streaming';
import { Subjects, Listener, UserUpdatedEvent } from "@internapsitude/commonrebates";
import { User } from '../../models/user';
import { queueGroupName } from './queue-group-name';

export class UserUpdatedListener extends Listener<UserUpdatedEvent> {
  subject: Subjects.UserUpdated = Subjects.UserUpdated;
  queueGroupName = queueGroupName;

  async onMessage(data: UserUpdatedEvent['data'], msg: Message) {
    const { id, email, password, role, version } = data;

    const user = await User.findOne({
      _id: id,
      version: version - 1
    });

    if(!user){
      throw new Error('User not found');
    }

    user.set("email",email);
    user.set("password",password);
    user.set("role",role);
    await user.save();

    msg.ack();
  }
}