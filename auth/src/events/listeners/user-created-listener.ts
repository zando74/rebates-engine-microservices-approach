import { Message } from 'node-nats-streaming';
import { Subjects, Listener, UserCreatedEvent } from "@internapsitude/commonrebates";
import { User } from '../../models/user';
import { queueGroupName } from './queue-group-name';

export class UserCreatedListener extends Listener<UserCreatedEvent> {
  subject: Subjects.UserCreated = Subjects.UserCreated;
  queueGroupName = queueGroupName;

  async onMessage(data: UserCreatedEvent['data'], msg: Message) {
    const { id, email, password, role } = data;
    const user = User.build({
      id,
      email, 
      password, 
      role
    });
    await user.save();

    msg.ack();
  }
}