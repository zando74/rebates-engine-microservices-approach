import { Message } from 'node-nats-streaming';
import { Subjects, Listener, UserDeletedEvent } from "@internapsitude/commonrebates";
import { User } from '../../models/user';
import { queueGroupName } from './queue-group-name';

export class UserDeletedListener extends Listener<UserDeletedEvent> {
  subject: Subjects.UserDeleted = Subjects.UserDeleted;
  queueGroupName = queueGroupName;

  async onMessage(data: UserDeletedEvent['data'], msg: Message) {
    const { id } = data;
    const user = await User.findById(id);

    if(!user){
      throw new Error('User not found');
    }

    await user.delete();

    msg.ack();
  }
}