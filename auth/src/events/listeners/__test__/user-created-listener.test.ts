import { UserCreatedListener } from '../user-created-listener';
import { UserCreatedEvent } from '@internapsitude/commonrebates';
import { natsWrapper } from '../../../nats-wrapper';
import { Message } from 'node-nats-streaming';
import mongoose from 'mongoose';
import { Password } from '../../../services/password';
import { User } from '../../../models/user';

const setup = async () => {
  const listener = new UserCreatedListener(natsWrapper.client);

  const data: UserCreatedEvent['data'] = {
    version: 0,
    id: new mongoose.Types.ObjectId().toHexString(),
    email: 'email@test.com',
    password: await Password.toHash('arandompassword'),
    role: 'ADMIN'
  }

  // @ts-ignore
  const msg: Message = {
    ack: jest.fn()
  };

  return { listener, data, msg };
};

it('creates and saves a user', async () => {
  const { listener, data, msg } = await setup();

  await listener.onMessage(data,msg);

  const user = await User.findById(data.id);

  expect(user).toBeDefined();
  expect(user!.email).toEqual(user!.email);
});

it('acks the message', async () => {
  const { listener, data, msg } = await setup();

  await listener.onMessage(data,msg);

  expect(msg.ack).toHaveBeenCalled();

});