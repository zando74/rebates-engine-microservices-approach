import { UserUpdatedListener } from '../user-updated-listener';
import { UserUpdatedEvent } from '@internapsitude/commonrebates';
import { natsWrapper } from '../../../nats-wrapper';
import { Message } from 'node-nats-streaming';
import { User } from '../../../models/user';
import mongoose from 'mongoose';
import { Password } from '../../../services/password';

const setup = async () => {

  const listener = new UserUpdatedListener(natsWrapper.client);

  const user = User.build({ 
    id: mongoose.Types.ObjectId().toHexString(),
    email: 'testemail@test.fr',
    password: await Password.toHash('arandompassword'),
    role: 'ADMIN'
  });

  await user.save();

  const data: UserUpdatedEvent['data'] = {
    id: user.id,
    email: 'updatedemail@test.fr',
    password: user.password,
    role: user.role,
    version: user.version + 1
  };

  // @ts-ignore
  const msg: Message = {
    ack: jest.fn()
  };

  return { msg, data, user, listener };
};




it('finds, updates, and saves a user', async () => {
  const { msg, data, user, listener } = await setup();

  await listener.onMessage(data, msg);

  const updatedUser = await User.findById(user.id);

  expect(updatedUser!.email).toEqual(data.email);
  expect(updatedUser!.version).toEqual(data.version);
});

it('acks the message', async () => {
  const { msg, data, user, listener } = await setup();

  await listener.onMessage(data, msg);

  expect(msg.ack).toHaveBeenCalled();
})

it('does not call ack if the event comes before another ', async () => {
  const { msg, data, listener, user } = await setup();

  data.version = 10;
  try{
    await listener.onMessage(data, msg);
  }catch(err) {

  }

  expect(msg.ack).not.toHaveBeenCalled();
  
})

