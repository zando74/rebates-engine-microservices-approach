import { UserDeletedListener } from '../user-deleted-listener';
import { UserDeletedEvent } from '@internapsitude/commonrebates';
import { natsWrapper } from '../../../nats-wrapper';
import { Message } from 'node-nats-streaming';
import { User } from '../../../models/user';
import mongoose from 'mongoose';
import { Password } from '../../../services/password';

const setup = async () => {

  const listener = new UserDeletedListener(natsWrapper.client);

  const user = User.build({ 
    id: mongoose.Types.ObjectId().toHexString(),
    email: 'testemail@test.fr',
    password: await Password.toHash('arandompassword'),
    role: 'ADMIN'
  });

  await user.save();

  const data: UserDeletedEvent['data'] = {
    id: user.id,
    version: user.version
  };

  // @ts-ignore
  const msg: Message = {
    ack: jest.fn()
  };

  return { msg, data, user, listener };
};

it('finds and delete a user on event', async () => {
  const { msg, data, user, listener } = await setup();

  await listener.onMessage(data, msg);

  const deletedUser = await User.findById(user.id);

  expect(deletedUser).toEqual(null);

});