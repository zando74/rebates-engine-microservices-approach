import request from 'supertest';
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import { app } from '../app';
import { User } from '../models/user';
import { Password } from '../services/password';


declare global {
  namespace NodeJS {
    interface Global {
      signin(): Promise<string[]>
    }
  }
}

let mongo: any;

jest.mock('../nats-wrapper');
jest.setTimeout(30000);

beforeAll(async () => {

  process.env.JWT_KEY = "cledetest";

  mongo = new MongoMemoryServer();
  const mongoUri = await mongo.getUri();

  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  
});

beforeEach(async () => {
  jest.clearAllMocks();
  const collections = await mongoose.connection.db.collections();
  for(let collection of collections){
    await collection.deleteMany({});
  }
  const user = User.build({ id: new mongoose.Types.ObjectId().toHexString(), email: "SuperAdmin@apsitude.com", password: await Password.toHash("adminRebates"), role: "ADMIN" });
  await user.save();
});

afterAll(async () => {
  await mongo.stop();
  await mongoose.connection.close();
});

global.signin = async () => {
  const email = 'SuperAdmin@apsitude.com';
  const password = 'adminRebates';

  const response = await request(app)
    .post('/api/auth/signin')
    .send({
      email, password
    })
    .expect(200);

  const cookie = response.get('Set-Cookie');
  return cookie;
}