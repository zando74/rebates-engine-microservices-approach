import mongoose from 'mongoose';
import { Password } from '../services/password';
import { updateIfCurrentPlugin } from 'mongoose-update-if-current';

// An interface that describes the properties
// that are required to create a new User
interface UserAttrs {
  id: string,
  email: string;
  password: string;
  role: string;
};

// An interface that describes the properties
// that a User Model has
interface UserModel extends mongoose.Model<UserDoc> {
  build(attrs: UserAttrs): UserDoc;
  findByEvent(event: { id: string, version: number }) : Promise<UserDoc | null >;
}

// An interface that describes the properties
// that a User Document has
interface UserDoc extends mongoose.Document {
  email: string,
  password: string;
  role: string;
  version: number;
}

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true
  }
}, {
  toJSON: {
    transform(doc, ret) {
      ret.id = ret._id;
      delete ret._id;
      delete ret.password;
      delete ret.__v;
    }
  }
});

userSchema.set('versionKey','version');
userSchema.plugin(updateIfCurrentPlugin);

userSchema.pre('save', async function(done) {
  done();
});

// doesn't work don't know why have to fix it, don't use it !
userSchema.statics.findByEvent = (event: {id: string, version: number }) => {
  return User.findOne({
    _id: event.id,
    verion: event.version - 1
  });
}

userSchema.statics.build = (attrs: UserAttrs) => {
  return new User({
    _id: attrs.id,
    email: attrs.email,
    password: attrs.password,
    role: attrs.role
  });
}

const User = mongoose.model<UserDoc, UserModel>('User', userSchema);

export { User };